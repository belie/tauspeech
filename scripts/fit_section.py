#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 10 08:39:16 2022

@author: benjamin
"""

import tauspeech
import argparse
import h5py
import time
import os

parser = argparse.ArgumentParser(description="Automatic fitting")
parser.add_argument("data_file", metavar="data_file", type=str,
                    help="File containing the section to analyze")
parser.add_argument("-o", dest="output", metavar="output", type=str,
                    help="output file (.pickle)")
parser.add_argument("-sg", dest="sigma", metavar="sigma", type=int,
                    default=4,
                    help="Gaussian filtering order (default=4)")
parser.add_argument("-m", dest="model", metavar="model", type=str,
                    default="tau",
                    help="""Model for fitting. To choose between
                    [tau (default), stam, scdo, gcdo]""")
parser.add_argument("-f", dest="fitting", metavar="fitted signal", type=str,
                    default="position",
                    help="Signal to fit [position (default), velocity or acceleration]")
parser.add_argument("-a", dest="velocity_weight", metavar="velocity_weight",
                    type=float, default=0,
                    help="""Weight applied to the velocity in
                    the objective function (default is 0 => no velocity weight)""")
parser.add_argument("-no", dest="model_order", metavar="model order",
                    type=int, default=6,
                    help=""" Order of the model for STAM (default is 6). 
                    Only applies for STAM.""")
parser.add_argument("-nj", dest="number_jobs", metavar="number of jobs", 
                    type=int, default=1, 
                    help="""Number of parallel runs to find the best
                    local minimum (default is 1). 
                    Only applies for methods STAM, S-CDO, and G-CDO if nr>1.""")
parser.add_argument("-nr", dest="number_simulations", 
                    metavar="number of simulations", type=int, default=100,
                    help="""Number of optimization runs to find the 
                    best local minimum (default is 100). 
                    Only applies for methods STAM, S-CDO, and G-CDO.""")
parser.add_argument("-mx", dest="max_iter", 
                    metavar="Maximal number of iterations", type=int, 
                    default=200,
                    help="""Maximal number of iterations 
                    for minimization (default=200)""")
parser.add_argument("-p", dest="parallel", metavar="parallel", type=str, 
                    default="processes", 
                    help="""Type of parallelization 
                    (either `processes` or `threads`) (default is `threads`). 
                    Only applies for methods STAM, S-CDO, and G-CDO 
                    if nr>1 and nj>1.""")
parser.add_argument("-v", dest="verbosity", metavar="verbosity",
                    type=int, default=0,
                    help=""" Level of verbosity 
                    (default=0, no display of progress bar) """)

data_file = parser.parse_args().data_file
output_file = parser.parse_args().output
sigma = parser.parse_args().sigma
model = parser.parse_args().model
fit = parser.parse_args().fitting
nj = parser.parse_args().number_jobs
nb_simul = parser.parse_args().number_simulations
velocity_weight = parser.parse_args().velocity_weight
parall = parser.parse_args().parallel
max_iter = parser.parse_args().max_iter
verbosity = parser.parse_args().verbosity
model_order = parser.parse_args().model_order
sigma = parser.parse_args().sigma

file_ext = os.path.splitext(data_file)[-1]

if file_ext.lower() in [".h5", ".hdf5"]:
    with h5py.File(data_file, "r") as hf:
        chunk = hf["signal"][()]
        sr = hf["sampling_frequency"][()]    

if verbosity == 0:
    verbosity = False
    disable = True
else:
    verbosity = True
    disable = False

if verbosity:
    print("File: ", data_file)
    print("Model: ", model)
    disable = False

    if model in ["stam", "scdo", "gcdo"]:
        print("Number of simulations: ", nb_simul)
        print("Number of threads: ", nj)
        if nj > 1:
            print("Parallel process: ", parall)
    if model == "stam":
        print("Velocity weight: ", velocity_weight)
        print("Model order: ", model_order)
    print("Maximal number of iterationr: ", max_iter)

start = time.time()

if model == "scdo":
    model = "cdo"
    activation = "rect"
elif model == "gcdo":
    model = "cdo"
    activation = "gradual"
else:
    activation = "rect"

Traj = tauspeech.create_trajectory(chunk, sr)
Traj.fit_trajectory(sigma=sigma, model=model, nb_simul=nb_simul, 
                    maxiter=max_iter, nb_threads=nj, parall=parall,
                    disable=disable, activation=activation, 
                    N=model_order, a=velocity_weight, method="nelder-mead",
                    fit=fit)
if verbosity:
    print("Fit is done")
    print("Global error is ", 100*Traj.parameters.global_error, '%')
    print("Total elapsed time: ", time.time() - start)

Traj.export_solution(output_file)
    
if verbosity:
    print("Data successfully saved in ", output_file)
