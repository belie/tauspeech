import os
from pathlib import Path
import tempfile
import pytest
from tauspeech import (_config, hdf5tools,segmentation)

@pytest.fixture
def home():
    """Sets and returns $HOME to a temporary directory."""
    with tempfile.TemporaryDirectory() as temphome:
        old_home = os.environ.get('HOME')
        os.environ['HOME'] = temphome
        try:
            yield temphome
        finally:
            if old_home:
                os.environ['HOME'] = old_home

@pytest.fixture
def datapath():
    """Returns the path to data directory alongside tests."""
    return _config.get_tauspeech_path() / 'tests' / 'data'

@pytest.fixture
def datafile():
    """Returns the path to test data file."""
    return _config.get_tauspeech_path() / 'tests' / 'data' / 'test_data.h5'

@pytest.fixture
def datachunk(datafile):
    """Returns the test data """    
    return hdf5tools.read_section(datafile)

@pytest.fixture
def data_trajectory(datachunk):
    """Returns the test data """    
    return segmentation.create_trajectory(datachunk[0], datachunk[1])


